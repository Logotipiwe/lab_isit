package sample.view;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import sample.AppContext;
import sample.MainApp;
import sample.model.Service;

import java.io.IOException;

public class First {
    @FXML
    GridPane gridPane;
    @FXML
    Button buttonAdd;
    @FXML
    Button buttonDel;
    @FXML
    Button buttonNext;
    //Добавление поля для ввода временного ограничения
    @FXML
    private void addRow(){
        int indexRow = gridPane.getRowIndex(buttonAdd);
        Label l = new Label("M"+(indexRow+1));
        l.setId("M"+(indexRow+1));
        TextField t = new TextField();
        t.setId("T"+(indexRow+1));

        gridPane.add(l, 0, indexRow);
        gridPane.add(t, 1, indexRow);
        buttonDel.setVisible(true);
        gridPane.getChildren().remove(buttonAdd);
        gridPane.add(buttonAdd, 2, indexRow+1);
        gridPane.getChildren().remove(buttonDel);
        gridPane.add(buttonDel, 2, indexRow);
    }

    //Удаление поля с временным ограничением
    @FXML
    private void deleteRow(){
        int indexRow = gridPane.getRowIndex(buttonDel);

        int to = gridPane.getChildren().size();
        int from = to - 4;

        gridPane.getChildren().remove(from, to);

        gridPane.add(buttonDel, 2, indexRow-1);
        gridPane.add(buttonAdd, 2, indexRow);

        if(to == 6){
            buttonDel.setVisible(false);
        }
    }

    //Запись значений временных ограничений и переход ко второму //окну приложения
    @FXML
    private void timeLimit()throws IOException {
        if (validate()){
            int timeLimit[] = new int[gridPane.getRowIndex(buttonAdd)];

            ObservableList<Node> childrens = gridPane.getChildren();
            TextField result = null;
            int i = 0;

            for(Node node:childrens){
                if (gridPane.getColumnIndex(node)!=null&&gridPane.getColumnIndex(node)==1){
                    result = (TextField) node;
                    String t = result.getText();
                    timeLimit[i] = Integer.valueOf(t);
                    i++;
                }
            }
            for (int j = 0; j < timeLimit.length; j++){
                AppContext.services.add(new Service(timeLimit[j]));
            }
            AppContext.mainApp.setSecondDecoration();
        }
    }

    //Проверка вводимых значений
    private boolean validate(){
        ObservableList<Node> childrens = gridPane.getChildren();
        TextField result = null;

        for(Node node:childrens){

            if (gridPane.getColumnIndex(node)!=null&&gridPane.getColumnIndex(node)==1){
                int res;
                result = (TextField) node;
                String t = result.getText();
                try {
                    res = Integer.valueOf(t);
                }catch(Exception e){

//                    mainApp.alert("Неверный формат параметров", "Ошибка ввода данных!", "Временное ограничение должно быть целым числом");

                    return false;
                }
                if (res <= 0){
//                    mainApp.alert("Неверный формат параметров", "Ошибка ввода данных!", "Временное ограничение не должно быть отрицательным числом или нулем");

                    return false;
                }
            }
        }
        return true;
    }

}
