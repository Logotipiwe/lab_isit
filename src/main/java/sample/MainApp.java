package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MainApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        AppContext.mainApp = this;
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("first.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }

    private void showScene(String name) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource(name));
        stage.setTitle("Hello World");
        stage.setScene(new Scene(root, 300, 275));
        stage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

    public void setSecondDecoration() throws IOException {
        showScene("second.fxml");
    }

    public void setFirstDecoration() throws IOException {
        showScene("first.fxml");
    }

    public void setThirdDecoration() throws IOException {
        showScene("third.fxml");
    }
}
